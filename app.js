var express = require('express');
var bodyParser = require('body-parser');
var unsecuredRoutes = require('./config/unsecured_routes');
var securedRoutes = require('./config/secured_routes');
var swaggerJSDoc = require('swagger-jsdoc')

var app = express();

// swagger definition
var swaggerDefinition = {
    info: {
        title: 'Node Swagger API',
        version: '1.0.0',
        description: 'Demonstrating how to describe a RESTful API with Swagger',
    },
    host: 'localhost:3000',
    basePath: '/',
};

// options for the swagger docs
var options = {
    // import swaggerDefinitions
    swaggerDefinition: swaggerDefinition,
    // path to the API docs
    apis: ['./config/*.js'],
};

// initialize swagger-jsdoc
var swaggerSpec = swaggerJSDoc(options);

app.use(bodyParser.json())
app.use(express.static('public'));
app.use('/', unsecuredRoutes);
app.use('/l/', securedRoutes);

app.get('/swagger.json', function(req, res) {
    res.setHeader('Content-Type', 'application/json');
    res.send(swaggerSpec);
});

app.listen(process.env.NODE_PORT, function() {
    console.log('Example app listening on port ' + process.env.NODE_PORT);
});