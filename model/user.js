var database = require('../config/db');

var userSchema = database.mongoose.Schema({
    id: String,
    created: Number,
    friends: [String],
    circles: [String],
    firstname: String,
    lastname: String,
    email: String
}, {
    collection: 'Users'
});

module.exports = {
    model: database.mongoose.model('user', userSchema),
    schema: userSchema
};