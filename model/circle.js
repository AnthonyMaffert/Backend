var database = require('../config/db');
var quote = require('./quote');

var circleSchema = database.mongoose.Schema({
    created: Number,
    deleted: {type: Number, default: null},
    owner: String,
    users_id: [String],
    name: String,
    quotes: [quote.schema]
}, {
    collection: 'Circles'
});

module.exports = {
    model: database.mongoose.model('circle', circleSchema),
    schema: circleSchema
};