var database = require('../config/db');

var quoteSchema = database.mongoose.Schema({
    created: Number,
    deleted: {type: Number, default: null},
    owner: String,
    introducer: String,
    sentence: String
});

module.exports = {
    model: database.mongoose.model('quote', quoteSchema),
    schema: quoteSchema
};