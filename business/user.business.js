var request = require('request');
var constants = require('../config/constants');
var SHA512 = require("crypto-js/sha512");

var UserDA = require('../dataaccess/user.dataaccess');
var jwtMiddleware = require('../middleware/jwt.middleware');

module.exports = {
    connect: function(req, res) {
        if (!req.params.access_token) {
            res.status(400).json({
                code: 400,
                message: "MISSING_PARAMETER"
            });

            return;
        }

        request(constants.facebookGraphApiUrl + 'me?access_token=' + req.params.access_token + '&fields=id%2Cfirst_name%2Clast_name%2Cemail&format=json&method=get&pretty=0&suppress_http_code=1', function(error, response, body) {
            var result = JSON.parse(body)
            if (!(result.id && result.first_name && result.last_name && result.email)) {
                res.status(400).json({
                    code: 400,
                    message: "BAD_TOKEN"
                });
                return;
            }
            var sha = 'fb' + SHA512(result.id);
            UserDA.getById(sha, function(user) {
                if (user == null) {
                    UserDA.create(sha, result.first_name, result.last_name, result.email, function(success) {
                        res.status(200).json({
                            'Quotify-Token': jwtMiddleware.createToken(sha)
                        });
                    }, function(error) {
                        res.status(500).json({
                            code: 500,
                            message: "INTERNAL_ERROR"
                        });
                    });
                } else {
                    res.status(200).json({
                        'Quotify-Token': jwtMiddleware.createToken(sha)
                    });
                }
            }, function(error) {
                res.status(500).json({
                    code: 500,
                    message: "INTERNAL_ERROR"
                });
            });
        });
    },

    addFriend: function(req, res) {
        if (!req.body.friend_id) {
            res.status(400).json({
                code: 400,
                message: "MISSING_PARAMETER_FRIEND_ID"
            });
            return;
        }
        if (req.userId == req.body.friend_id) {
            res.status(400).json({
                code: 400,
                message: "ADD_YOURSELF"
            });
        }
        UserDA.addFriend(req.userId, req.body.friend_id, function(success) {
            res.status(200).json({
                value: true
            })
        }, function(error) {
            res.status(500).json({
                code: 500,
                message: "FRIEND_ADD_FAIL"
            });
        })
    },

    editUser: function(req, res) {
        if (!(req.body.firstname || req.body.lastname)) {
            res.status(400).json({
                code: 400,
                message: "MISSING_PARAMETERS"
            });
            return;
        }
        UserDA.editUser(req.userId, req.body, function(success) {
            res.status(200).json({
                value: true
            })
        }, function(error) {
            res.status(500).json({
                code: 500,
                message: "USER_EDIT_FAIL"
            });
        })
    }
};