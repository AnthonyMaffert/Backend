var request = require('request');

var CircleDA = require('../dataaccess/circle.dataaccess');
var UserDA = require('../dataaccess/user.dataaccess');
var jwtMiddleware = require('../middleware/jwt.middleware');

module.exports = {
    findById: function(req, res) {
        if (!req.params.circle_id) {
            res.status(400).json({
                code: 400,
                message: "MISSING_PARAMETER_CIRCLE_ID"
            });
            return;
        }
        CircleDA.findById(req.params.circle_id, function(circle) {
            res.status(200).json({
                circle
            });
        }, function(error) {
            res.status(500).json({
                code: 500,
                message: "CIRCLE_GET_FAIL"
            });
        });
    },
    createCircle: function(req, res) {
        var body = req.body;
        if (!body.name) {
            res.status(400).json({
                code: 400,
                message: "MISSING_PARAMETER"
            });
            return;
        }
        CircleDA.create(body.name, req.userId, function(circle) {
            UserDA.addCircle(req.userId, circle.id, function(success) {
                res.status(200).json({
                    circle
                })
                return;
            }, function(error) {
                res.status(500).json({
                    code: 500,
                    message: "CIRCLE_ADD_FAIL"
                });
                return;
            })

        }, function(error) {
            res.status(500).json({
                code: 500,
                message: "CIRCLE_ADD_FAIL"
            });
            return;
        });

    },

    addFriends: function(req, res) {
        var body = req.body;
        if (!body.friend_ids) {
            res.status(400).json({
                code: 400,
                message: "MISSING_PARAMETER_FRIEND_IDS"
            });
            return;
        }
        if (!body.circle_id) {
            res.status(400).json({
                code: 400,
                message: "MISSING_PARAMETER_CIRCLE_ID"
            });
            return;
        }
        CircleDA.addFriends(body.circle_id, body.friend_ids, req.userId, function(success) {
            res.status(200).json({
                value: true
            });
            return;
        }, function(error) {
            res.status(500).json({
                code: 500,
                message: "CIRCLE_FIREND_ADD_FAIL"
            });
            return;
        });

    },

    addQuote: function(req, res) {
        if (!(req.body.circle_id && req.body.introducer_user_id && req.body.sentence)) {
            res.status(400).json({
                code: 400,
                message: "MISSING_PARAMETERS"
            });
            return;
        }
        CircleDA.addQuote(req.body.circle_id, req.userId, req.body.introducer_user_id, req.body.sentence, function(success) {
            res.status(200).json({
                value: true
            });
            return;
        }, function(error) {
            res.status(500).json({
                code: 500,
                message: "ADD_QUOTE_FAIL"
            });
            return;
        });
    }
};