var mongoose = require('mongoose');

mongoose.connect('mongodb://' + process.env.MONGO_URL + '/Quotify', {
    useMongoClient: true
});

var db = {
    mongoose: mongoose,
    db: mongoose.connection
};

module.exports = db;