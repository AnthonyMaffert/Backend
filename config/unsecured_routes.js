var express = require('express');
var router = express.Router();

var UserBusiness = require('../business/user.business');

/**
 * @swagger
 * definitions:
 *   MessageResponse:
 *     properties:
 *       Message:
 *         type: string
 */

/**
 * @swagger
 * /test:
 *   get:
 *     tags:
 *       - Test
 *     description: Returns a test statement
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: A test string
 *         schema:
 *           properties:
 *              message:
 *                  type: string
 */
router.get('/test', function(req, res) {
    res.status(200).json({
        message: "toto UNsecured"
    });
});

/**
 * @swagger
 * /connect/{facebook_id}:
 *   get:
 *     tags:
 *       - Users
 *     description: Retruns a session token
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: facebook_id
 *         description: Facebook Graph API User ID
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: The user session token
 *         schema:
 *           properties:
 *              Quotify-Token:
 *                  type: string
 *       400:
 *         description: Connection failed
 *         schema:
 *          properties:
 *             code:
 *               type: integer
 *             message:
 *               type: string
 */
router.get('/connect/:access_token', function(req, res) {
    UserBusiness.connect(req, res);
})

module.exports = router;