var express = require('express');
var router = express.Router();

var jwtMiddleWare = require('../middleware/jwt.middleware');
var CircleBusiness = require('../business/circle.business');
var UserBusiness = require('../business/user.business');

router.use(jwtMiddleWare.authenticate);

router.get('/test', function(req, res) {
    res.status(200).json({
        message: "toto secured"
    });
});

router.put('/user/addfriend', function(req, res) {
    UserBusiness.addFriend(req, res);
});

router.put('/circle/addfriends', function(req, res) {
    console.log("Allo");
    CircleBusiness.addFriends(req, res);
});

router.post('/circle', function(req, res) {
    CircleBusiness.createCircle(req, res);
});

router.get('/circle/:circle_id', function(req, res) {
    CircleBusiness.findById(req, res);
});

router.put('/user', function(req, res) {
    UserBusiness.editUser(req, res);
});

router.put('/circle/addQuote', function(req, res) {
    CircleBusiness.addQuote(req, res);
})

module.exports = router;