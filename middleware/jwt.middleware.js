var jwt = require('jsonwebtoken');

var constants = require('../config/constants');

module.exports = {
    authenticate: function(req, res, next) {
        if (!req.header(constants.headerName)) {
            res.status(403).json({
                code: 403,
                message: "FORBIDDEN"
            });
            res.end();
        }

        var token = req.header(constants.headerName);
        try {
            var decoded = jwt.verify(token, constants.secret);
            req.userId = decoded.userId;
            
            next();
        } catch (err) {
            res.status(403).json({
                code: 403,
                message: "FORBIDDEN"
            });
            res.end();
        }
    },

    createToken: function(userId) {
        return jwt.sign({
            userId: userId
        }, constants.secret);
    }
};