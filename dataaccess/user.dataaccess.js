var database = require('../config/db');
var User = require('../model/user');

module.exports = {
    getById: function(id, success, error) {
        User.model.findOne({ 'id': id }, function(err, user) {
            if (err) {
                error(err);
                return;
            }
            success(user);
        })
    },

    create: function(id, firstname, lastname, email, success, error) {
        var user = new User.model({
            id: id,
            created: Date.now(),
            firstname: firstname,
            lastname: lastname,
            email: email
        });

        user.save(function(err) {
            if (err) {
                error(err);
                return;
            }

            success(user);
        })
    },

    addCircle: function(userId, circleId, success, error) {
        User.model.update({ id: userId }, { $push: { circles: circleId } }, function(err, raw) {
            if (err) {
                error(err);
                return;
            }

            success(true);
        });
    },

    addFriend: function(owner, friend, success, error) {
        User.model.update({ id: owner }, { $addToSet: { friends: friend } }, function(err, raw) {
            if (err) {
                error(err);
                return;
            }

            success(true);
        })
    },

    editUser: function(userId, userData, success, error) {
        User.model.update({ id: userId }, {
            firstname: userData.firstname,
            lastname: userData.lastname,
        }, function(err, raw) {
            if (err) {
                error(err);
                return;
            }
            success(true);
        })
    }
}