var ObjectId = require('mongodb').ObjectID;

var database = require('../config/db');
var Circle = require('../model/circle');
var Quote = require('../model/quote');

module.exports = {
    findById: function(circle_id, success, error) {
        Circle.model.findOne({ _id: circle_id }, function(err, res) {
            if (err) {
                error(err);
                return;
            }
            success(res);
        })
    },
    create: function(name, owner, success, error) {
        var circle = new Circle.model({
            created: Date.now(),
            owner: owner,
            users_id: [owner],
            name: name,
            quotes: []
        });

        circle.save(function(err) {
            if (err) {
                error(err);
                return;
            }

            success(circle);
        })
    },

    addFriends: function(circleId, friend_ids, current_user_id, success, error) {
        var count = 0;
        for (count; count < friend_ids.length; count++) {
            if (friend_ids[count] == current_user_id) {
                continue;
            }
            console.log(circleId);
            console.log(friend_ids[count]);

            Circle.model.update({ _id: circleId }, { $addToSet: { users_id: friend_ids[count] } }, function(err, raw) {
                console.log(raw);
                if (err) {
                    error(err);
                    return;
                }
            });
        }

        success(true);
    },

    addQuote: function(circle_id, current_user_id, introducer_user_id, sentence, success, error) {
        var quote = new Quote.model({
            created: Date.now(),
            owner: current_user_id,
            introducer: introducer_user_id,
            sentence: sentence
        });
        Circle.model.update({ _id: circle_id }, { $push: { quotes: quote } }, function(err, raw) {
            console.log(raw);
            if (err) {
                error(err);
                return;
            }
            success(true);
        });
    }
}